//
//  LandingViewModelType.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/16/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

protocol LandingViewModelType: class {
    
    func login()
    
}


protocol LandingViewModelDelegate: class {
    
    func login()
    
}
