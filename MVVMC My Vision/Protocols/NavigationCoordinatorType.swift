//
//  NavigationCoordinatorType.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/18/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


protocol NavigationCoordinatorType: class {
    
    var navigationController: UINavigationController { get }
    
    func start(in containerViewController: UIViewController?, completionHandler: (_ animated: Bool) -> ())
    
}
