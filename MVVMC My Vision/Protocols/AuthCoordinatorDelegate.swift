//
//  AuthCoordinatorDelegate.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/26/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

protocol AuthCoordinatorDelegate: class {
    
    func userDidAuthorized()
    
}
