//
//  TutorialSecondViewModelType.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/17/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

protocol TutorialSecondViewModelType: class {
    
    func showNext()
    
}


protocol TutorialSecondViewModelDelegate: class {
    
    func showNext()
    
}
