//
//  SecondTabSecondViewModelType.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/18/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

protocol SecondTabSecondViewModelType: class {
    
    func showTutorial()
    
}


protocol SecondTabSecondViewModelDelegate {
    
    func showTutorial()
    
}
