//
//  CoordinatorType.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/16/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


protocol CoordinatorType: class {
    
    func start()
    
}
