//
//  FirstTabSecondViewModelType.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/17/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

protocol FirstTabSecondViewModelType: class {
    
    func logout()
    
}


protocol FirstTabSecondViewModelDelegate: class {
    
    func logout()
    
}
