//
//  MainTabBarCoordinatorDelegate.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/27/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

protocol MainTabBarCoordinatorDelegate: class {
    
    func logout()
    
}
