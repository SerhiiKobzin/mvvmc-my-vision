//
//  TutorialFirstViewModelType.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/17/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

protocol TutorialFirstViewModelType: class {
    
    func showNext()
    
}


protocol TutorialFirstViewModelDelegate: class {
    
    func showNext()
    
}
