//
//  SecondTabSecondViewController.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/18/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


class SecondTabSecondViewController: UIViewController {

    private let viewModel: SecondTabSecondViewModelType
    
    init(viewModel: SecondTabSecondViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    @IBAction func buttonTouchUpInside(_ sender: UIButton) {
        viewModel.showTutorial()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
