//
//  SecondTabSecondViewModel.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/18/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

class SecondTabSecondViewModel: SecondTabSecondViewModelType {
    
    private var delegate: SecondTabSecondViewModelDelegate
    
    init(delegate: SecondTabSecondViewModelDelegate) {
        self.delegate = delegate
    }
    
    func showTutorial() {
        delegate.showTutorial()
    }
    
    deinit {
        print("---> \(String(describing: self)): deinit")
    }
    
}
