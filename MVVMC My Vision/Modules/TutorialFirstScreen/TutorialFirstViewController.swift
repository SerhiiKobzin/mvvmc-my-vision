//
//  TutorialFirstViewController.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/17/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


class TutorialFirstViewController: UIViewController {
    
    private let viewModel: TutorialFirstViewModelType
    
    init(viewModel: TutorialFirstViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func buttonTouchUpInside(_ sender: UIButton) {
        viewModel.showNext()
    }
    
}
