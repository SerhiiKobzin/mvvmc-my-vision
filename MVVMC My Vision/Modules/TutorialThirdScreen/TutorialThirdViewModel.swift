//
//  TutorialThirdViewModel.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/17/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

class TutorialThirdViewModel: TutorialThirdViewModelType {
    
    private let delegate: TutorialThirdViewModelDelegate
    
    init(delegate: TutorialThirdViewModelDelegate) {
        self.delegate = delegate
    }
    
    func close() {
        delegate.close()
    }
    
    deinit {
        print("---> \(String(describing: self)): deinit")
    }
    
}
