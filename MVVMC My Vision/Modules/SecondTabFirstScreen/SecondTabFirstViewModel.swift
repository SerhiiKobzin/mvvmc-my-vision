//
//  SecondTabFirstViewModel.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/17/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

class SecondTabFirstViewModel: SecondTabFirstViewModelType {
    
    private let delegate: SecondTabFirstViewModelDelegate
    
    init(delegate: SecondTabFirstViewModelDelegate) {
        self.delegate = delegate
    }
    
    func showNext() {
        delegate.showNext()
    }
    
    deinit {
        print("---> \(String(describing: self)): deinit")
    }
    
}
