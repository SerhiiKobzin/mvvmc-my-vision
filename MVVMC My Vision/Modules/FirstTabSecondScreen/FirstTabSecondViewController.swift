//
//  FirstTabSecondViewController.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/17/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


class FirstTabSecondViewController: UIViewController {
    
    private let viewModel: FirstTabSecondViewModelType
    
    init(viewModel: FirstTabSecondViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func buttonTouchUpInside(_ sender: UIButton) {
        viewModel.logout()
    }
    
}
