//
//  FirstTabSecondViewModel.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/17/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

class FirstTabSecondViewModel: FirstTabSecondViewModelType {
    
    private var delegate: FirstTabSecondViewModelDelegate
    
    init(delegate: FirstTabSecondViewModelDelegate) {
        self.delegate = delegate
    }
    
    func logout() {
        delegate.logout()
    }
    
    deinit {
        print("---> \(String(describing: self)): deinit")
    }
    
}
