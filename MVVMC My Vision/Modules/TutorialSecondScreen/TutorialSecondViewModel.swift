//
//  TutorialSecondViewModel.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/17/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

class TutorialSecondViewModel: TutorialSecondViewModelType {
    
    private let delegate: TutorialSecondViewModelDelegate
    
    init(delegate: TutorialSecondViewModelDelegate) {
        self.delegate = delegate
    }
    
    func showNext() {
        delegate.showNext()
    }
    
    deinit {
        print("---> \(String(describing: self)): deinit")
    }
    
}
