//
//  FirstTabFirstViewModel.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/17/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

class FirstTabFirstViewModel: FirstTabFirstViewModelType {
    
    private let delegate: FirstTabFirstViewModelDelegate
    
    init(delegate: FirstTabFirstViewModelDelegate) {
        self.delegate = delegate
    }
    
    func showNext() {
        delegate.showNext()
    }
    
    deinit {
        print("---> \(String(describing: self)): deinit")
    }
    
}
