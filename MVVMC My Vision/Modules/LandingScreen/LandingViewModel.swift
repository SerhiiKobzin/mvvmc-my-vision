//
//  LandingViewModel.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/16/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

class LandingViewModel: LandingViewModelType {
    
    private let delegate: LandingViewModelDelegate
    
    init(delegate: LandingViewModelDelegate) {
        self.delegate = delegate
    }
    
    func login() {
        delegate.login()
    }
    
    deinit {
        print("---> \(String(describing: self)): deinit")
    }
    
}
