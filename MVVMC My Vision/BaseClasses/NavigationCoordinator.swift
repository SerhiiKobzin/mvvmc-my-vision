//
//  NavigationCoordinator.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/25/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


class NavigationCoordinator: NavigationCoordinatorType {
    
    var navigationController = UINavigationController()
    
    private var lastViewController: UIViewController?
    
    init() {
        print("---> \(self): init")
    }
    
    func start(in containerViewController: UIViewController? = nil, completionHandler: (_ animated: Bool) -> ()) {
        if let containerViewController = containerViewController as? UINavigationController {
            navigationController = containerViewController
            lastViewController = navigationController.viewControllers.last
            completionHandler(true)
        } else if let containerViewController = containerViewController as? UITabBarController {
            completionHandler(false)
            if let viewControllers = containerViewController.viewControllers {
                containerViewController.viewControllers = viewControllers + [navigationController]
            } else {
                containerViewController.viewControllers = [navigationController]
            }
        } else if containerViewController == nil, let appDelegate = UIApplication.shared.delegate as? AppDelegate, let window = appDelegate.window {
            completionHandler(false)
            window.rootViewController = navigationController
            window.makeKeyAndVisible()
        } else {
            print("---> \(self): fail start")
        }
    }
    
    func popViewController(animated: Bool) {
        navigationController.popViewController(animated: animated)
    }
    
    func popViewControllers(number: Int, animated: Bool) {
        let viewControllers = navigationController.viewControllers
        if number >= viewControllers.count {
            popAllViewControllers(animated: animated)
            return
        }
        navigationController.popToViewController(viewControllers[viewControllers.count - number - 1], animated: animated)
    }
    
    func popAllViewControllers(animated: Bool) {
        guard let lastViewController = self.lastViewController else {
            navigationController.viewControllers.removeAll()
            return
        }
        navigationController.popToViewController(lastViewController, animated: animated)
    }
    
    deinit {
        print("---> \(self): deinit")
    }
    
}
