//
//  TabBarCoordinator.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/25/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


class TabBarCoordinator: TabBarCoordinatorType {
    
    let tabBarController = UITabBarController()
    
    init() {
        print("---> \(self): init")
    }
    
    func start(in containerViewController: UIViewController? = nil, completionHandlers: [() -> ()]) {
        if let containerViewController = containerViewController as? UINavigationController {
            for completionHandler in completionHandlers {
                completionHandler()
            }
            containerViewController.pushViewController(tabBarController, animated: true)
        } else if containerViewController == nil, let appDelegate = UIApplication.shared.delegate as? AppDelegate, let window = appDelegate.window {
            for completionHandler in completionHandlers {
                completionHandler()
            }
            window.rootViewController = tabBarController
            window.makeKeyAndVisible()
        } else {
            print("---> \(self): fail start")
        }
    }
    
    func removeAllViewControllers() {
        tabBarController.viewControllers?.removeAll()
    }
    
    deinit {
        print("---> \(self): deinit")
    }
    
}
