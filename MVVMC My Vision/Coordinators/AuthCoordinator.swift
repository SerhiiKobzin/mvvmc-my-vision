//
//  AuthCoordinator.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/17/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


class AuthCoordinator: NavigationCoordinator {
    
    private let delegate: AuthCoordinatorDelegate
    
    init(delegate: AuthCoordinatorDelegate) {
        self.delegate = delegate
        super.init()
    }
    
    func start() {
        if UserDefaultsService.didUserAuthorized {
            delegate.userDidAuthorized()
            return
        }
        super.start(completionHandler: pushLandingViewController)
    }
    
    private func pushLandingViewController(animated: Bool) {
        let viewModel = LandingViewModel(delegate: self)
        let viewController = LandingViewController(viewModel: viewModel)
        navigationController.pushViewController(viewController, animated: animated)
    }
    
}


extension AuthCoordinator: LandingViewModelDelegate {
    
    func login() {
        popAllViewControllers(animated: true)
        UserDefaultsService.didUserAuthorized = true
        delegate.userDidAuthorized()
    }
    
}
