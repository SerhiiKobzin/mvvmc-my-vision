//
//  SecondTabCoordinator.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/17/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


class SecondTabCoordinator: NavigationCoordinator {
    
    override init() {
        super.init()
        navigationController.tabBarItem = UITabBarItem(tabBarSystemItem: .downloads, tag: 1)
    }
    
    func start(in containerViewController: UIViewController? = nil) {
        super.start(in: containerViewController, completionHandler: pushFirstViewController)
    }
    
    private func pushFirstViewController(animated: Bool) {
        let viewModel = SecondTabFirstViewModel(delegate: self)
        let viewController = SecondTabFirstViewController(viewModel: viewModel)
        navigationController.pushViewController(viewController, animated: animated)
    }
    
    private func pushSecondViewController(animated: Bool) {
        let viewModel = SecondTabSecondViewModel(delegate: self)
        let viewController = SecondTabSecondViewController(viewModel: viewModel)
        navigationController.pushViewController(viewController, animated: animated)
    }
    
}


extension SecondTabCoordinator: SecondTabFirstViewModelDelegate {
    
    func showNext() {
        pushSecondViewController(animated: true)
    }
    
}


extension SecondTabCoordinator: SecondTabSecondViewModelDelegate {
    
    func showTutorial() {
        TutorialCoordinator(delegate: self).start(in: navigationController)
    }
    
}


extension SecondTabCoordinator: TutorialCoordinatorDelegate {
    
    func tutorialWasShown() {
        
    }
    
}
