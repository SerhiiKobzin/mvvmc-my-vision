//
//  AppCoordinator.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/16/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


class AppCoordinator: CoordinatorType {
    
    init() {
        print("---> \(self): init")
    }
    
    func start() {
        startTutorialCoordinator()
    }
    
    private func startTutorialCoordinator() {
        TutorialCoordinator(delegate: self).start()
    }
    
    private func startAuthCoordinator() {
        AuthCoordinator(delegate: self).start()
    }
    
    private func startMainTabBarCoordinator() {
        MainTabBarCoordinator(delegate: self).start()
    }
    
    deinit {
        print("---> \(self): deinit")
    }
    
}


extension AppCoordinator: TutorialCoordinatorDelegate {
    
    func tutorialWasShown() {
        startAuthCoordinator()
    }
    
}


extension AppCoordinator: AuthCoordinatorDelegate {
    
    func userDidAuthorized() {
        startMainTabBarCoordinator()
    }
    
}


extension AppCoordinator: MainTabBarCoordinatorDelegate {
    
    func logout() {
        startAuthCoordinator()
    }
    
}
