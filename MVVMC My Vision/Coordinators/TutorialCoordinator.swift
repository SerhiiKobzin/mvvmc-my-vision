//
//  TutorialCoordinator.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/17/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


class TutorialCoordinator: NavigationCoordinator {
    
    private let delegate: TutorialCoordinatorDelegate
    
    init(delegate: TutorialCoordinatorDelegate) {
        self.delegate = delegate
        super.init()
    }
    
    func start(in containerViewController: UIViewController? = nil) {
        if UserDefaultsService.needShowTutorial || containerViewController != nil {
            super.start(in: containerViewController, completionHandler: pushFirstViewController)
            return
        }
        delegate.tutorialWasShown()
    }
    
    private func pushFirstViewController(animated: Bool) {
        let viewModel = TutorialFirstViewModel(delegate: self)
        let viewController = TutorialFirstViewController(viewModel: viewModel)
        navigationController.pushViewController(viewController, animated: animated)
    }
    
    private func presentFirstViewController(animated: Bool) {
        let viewModel = TutorialFirstViewModel(delegate: self)
        let viewController = TutorialFirstViewController(viewModel: viewModel)
        navigationController.present(viewController, animated: animated)
    }
    
    private func pushSecondViewController(animated: Bool) {
        let viewModel = TutorialSecondViewModel(delegate: self)
        let viewController = TutorialSecondViewController(viewModel: viewModel)
        navigationController.pushViewController(viewController, animated: animated)
    }
    
    private func pushThirdViewController(animated: Bool) {
        let viewModel = TutorialThirdViewModel(delegate: self)
        let viewController = TutorialThirdViewController(viewModel: viewModel)
        navigationController.pushViewController(viewController, animated: animated)
    }
    
}


extension TutorialCoordinator: TutorialFirstViewModelDelegate, TutorialSecondViewModelDelegate {
    
    func showNext() {
        if navigationController.viewControllers.last as? TutorialFirstViewController != nil {
            pushSecondViewController(animated: true)
        } else if navigationController.viewControllers.last as? TutorialSecondViewController != nil {
            pushThirdViewController(animated: true)
        }
    }
    
}


extension TutorialCoordinator: TutorialThirdViewModelDelegate {
    
    func close() {
        popAllViewControllers(animated: true)
        UserDefaultsService.needShowTutorial = false
        delegate.tutorialWasShown()
    }
    
}
