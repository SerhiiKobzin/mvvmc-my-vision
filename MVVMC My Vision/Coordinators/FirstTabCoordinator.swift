//
//  FirstTabCoordinator.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/16/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


class FirstTabCoordinator: NavigationCoordinator {
    
    private let delegate: FirstTabCoordinatorDelegate
    
    init(delegate: FirstTabCoordinatorDelegate) {
        self.delegate = delegate
        super.init()
        navigationController.tabBarItem = UITabBarItem(tabBarSystemItem: .favorites, tag: 0)
    }
    
    func start(in containerViewController: UIViewController? = nil) {
        super.start(in: containerViewController, completionHandler: pushFirstViewController)
    }
    
    private func pushFirstViewController(animated: Bool) {
        let viewModel = FirstTabFirstViewModel(delegate: self)
        let viewController = FirstTabFirstViewController(viewModel: viewModel)
        navigationController.pushViewController(viewController, animated: animated)
    }
    
    private func pushSecondViewController(animated: Bool) {
        let viewModel = FirstTabSecondViewModel(delegate: self)
        let viewController = FirstTabSecondViewController(viewModel: viewModel)
        navigationController.pushViewController(viewController, animated: animated)
    }
    
}


extension FirstTabCoordinator: FirstTabFirstViewModelDelegate {
    
    func showNext() {
        pushSecondViewController(animated: true)
    }
    
}


extension FirstTabCoordinator: FirstTabSecondViewModelDelegate {
    
    func logout() {
        popAllViewControllers(animated: false)
        delegate.logout()
    }
    
}
