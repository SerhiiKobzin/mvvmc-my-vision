//
//  MainTabBarCoordinator.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/16/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import UIKit


class MainTabBarCoordinator: TabBarCoordinator {
    
    private let delegate: MainTabBarCoordinatorDelegate
    
    init(delegate: MainTabBarCoordinatorDelegate) {
        self.delegate = delegate
    }
    
    func start(in containerViewController: UIViewController? = nil) {
        super.start(in: containerViewController, completionHandlers: [startFirstTabCoordinator, startSecondTabCoordinator])
    }
    
    private func startFirstTabCoordinator() {
        FirstTabCoordinator(delegate: self).start(in: tabBarController)
    }
    
    private func startSecondTabCoordinator() {
        SecondTabCoordinator().start(in: tabBarController)
    }
    
}


extension MainTabBarCoordinator: FirstTabCoordinatorDelegate {
    
    func logout() {
        removeAllViewControllers()
        UserDefaultsService.didUserAuthorized = false
        delegate.logout()
    }
    
}
