//
//  UserDefaultsService.swift
//  MVVMC My Vision
//
//  Created by Serhii Kobzin on 4/17/18.
//  Copyright © 2018 Onix-Systems. All rights reserved.
//

import Foundation


class UserDefaultsService {
    
    static var needShowTutorial: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: "needShowTutorial")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: "needShowTutorial") as? Bool ?? true
        }
    }
    
    static var didUserAuthorized: Bool {
        set {
            UserDefaults.standard.set(newValue, forKey: "didUserAuthorized")
            UserDefaults.standard.synchronize()
        }
        get {
            return UserDefaults.standard.object(forKey: "didUserAuthorized") as? Bool ?? false
        }
    }

}
